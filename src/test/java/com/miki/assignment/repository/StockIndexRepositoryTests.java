package com.miki.assignment.repository;

import com.miki.assignment.domain.StockIndex;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.nullValue;
import static org.hamcrest.core.Is.is;

/**
 * Created by worawat on 3/25/2017 AD.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class StockIndexRepositoryTests {
    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private StockIndexRepository repository;

    @Test
    public void findAllByNameAndRetrievedDateAfter() {
        List<StockIndex> entities = givenStockIndices();
        LocalDateTime fromDate = LocalDateTime.parse("2017-03-26T00:00:00");
        LocalDateTime toDate = LocalDateTime.parse("2017-03-26T10:01:00");
        List<StockIndex> stockIndices = repository.findAllByNameAndRetrievedDateBetween("SET50", fromDate, toDate);
        assertThat(stockIndices, hasSize(2));
        assertThat(stockIndices, hasItem(entities.get(1)));
        assertThat(stockIndices, hasItem(entities.get(2)));
    }

    @Test
    public void findByNameAndRetrievedDate() {
        givenStockIndices();
        LocalDateTime time = LocalDateTime.parse("2017-03-26T10:01:00");
        StockIndex entity = repository.findByNameAndRetrievedDate("SET50", time);
        assertThat(entity.getName(), equalTo("SET50"));
        assertThat(entity.getLast(), equalTo(1300.74));
        assertThat(entity.getRetrievedDate(), equalTo(LocalDateTime.parse("2017-03-26T10:01:00")));
    }

    @Test
    public void findByNameAndRetrievedDateShouldReturnNull() {
        givenStockIndices();
        LocalDateTime time = LocalDateTime.parse("2017-03-26T10:01:00");
        StockIndex entity = repository.findByNameAndRetrievedDate("SET", time);
        assertThat(entity, is(nullValue()));
    }

    private List<StockIndex> givenStockIndices() {
        List<StockIndex> entities = Arrays.asList(
                new StockIndex("SET50", 1230.74, LocalDateTime.parse("2017-03-25T23:01:00")),
                new StockIndex("SET50", 1230.74, LocalDateTime.parse("2017-03-26T00:01:00")),
                new StockIndex("SET50", 1300.74, LocalDateTime.parse("2017-03-26T10:01:00")),
                new StockIndex("SET50", 1278.74, LocalDateTime.parse("2017-03-27T01:01:00")),
                new StockIndex("SET", 1230.74, LocalDateTime.parse("2017-03-26T00:01:00"))
        );
        entities.forEach(entityManager::persist);
        return entities;
    }
}
