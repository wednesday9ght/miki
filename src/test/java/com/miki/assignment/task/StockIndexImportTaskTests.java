package com.miki.assignment.task;

import com.miki.assignment.collector.StockIndexCollector;
import com.miki.assignment.collector.StockIndexCollectorTests;
import com.miki.assignment.domain.StockIndex;
import com.miki.assignment.repository.StockIndexRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * Created by worawat on 3/26/2017 AD.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class StockIndexImportTaskTests {
    @Autowired
    private StockIndexImportTask importTask;
    @MockBean
    StockIndexCollector collector;
    @MockBean
    StockIndexRepository repository;

    @Test
    public void importStockIndices() {
        List<StockIndex> stockIndices = Arrays.asList(
                new StockIndex("SET", 1230.74, LocalDateTime.parse("2017-03-25T23:01:00")),
                new StockIndex("SET50", 1230.74, LocalDateTime.parse("2017-03-25T23:01:00")),
                new StockIndex("SET100", 1230.74, LocalDateTime.parse("2017-03-25T23:01:00"))
        );
        given(collector.retrieveStockIndices()).willReturn(stockIndices);

        importTask.importStockIndices();

        verify(repository).save(stockIndices);
    }
}
