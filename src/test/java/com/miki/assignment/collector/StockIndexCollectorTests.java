package com.miki.assignment.collector;

import com.miki.assignment.domain.StockIndex;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

/**
 * Created by worawat on 3/26/2017 AD.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class StockIndexCollectorTests {
    @Autowired
    private StockIndexCollector collector;

    @Test
    public void retrieveStockIndices() {
        List<StockIndex> indices = collector.retrieveStockIndices();
        assertThat(indices, hasSize(6));
    }
}
