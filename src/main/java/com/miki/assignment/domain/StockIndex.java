package com.miki.assignment.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Created by worawat on 3/25/2017 AD.
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"id", "createdDate", "modifiedDate"})
public class StockIndex {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "last")
    private Double last;
    @Column(name = "retrieved_date")
    private LocalDateTime retrievedDate;
    @Column(name = "created_date", nullable = false, updatable = false)
    @CreatedDate
    private Date createdDate;
    @Column(name = "modified_date")
    @LastModifiedDate
    private Date modifiedDate;

    public StockIndex() {
    }

    public StockIndex(String name, Double last, LocalDateTime retrievedDate) {
        this.name = name;
        this.last = last;
        this.retrievedDate = retrievedDate;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getLast() {
        return last;
    }

    public LocalDateTime getRetrievedDate() {
        return retrievedDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLast(Double last) {
        this.last = last;
    }

    public void setRetrievedDate(LocalDateTime retrievedDate) {
        this.retrievedDate = retrievedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (null == o) {
            return false;
        }

        if (this == o) {
            return true;
        }

        if (!o.getClass().equals(this.getClass())) {
            return false;
        }
        StockIndex that = (StockIndex) o;
        if (this.getId() != null && that.getId() != null) {
            return this.getId().equals(that.getId());
        }
        return new EqualsBuilder()
                .append(this.getName(), that.getName())
                .append(this.getLast(), that.getLast())
                .append(this.getRetrievedDate(), that.getRetrievedDate())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(this.getId())
                .append(this.getName())
                .append(this.getLast())
                .append(this.getRetrievedDate())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append(this.getId())
                .append(this.getName())
                .append(this.getLast())
                .append(this.getRetrievedDate())
                .toString();
    }
}
