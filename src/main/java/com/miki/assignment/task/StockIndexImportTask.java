package com.miki.assignment.task;

import com.miki.assignment.collector.StockIndexCollector;
import com.miki.assignment.config.ApplicationProperties;
import com.miki.assignment.domain.StockIndex;
import com.miki.assignment.repository.StockIndexRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by worawat on 3/25/2017 AD.
 */
@Component
@Transactional
public class StockIndexImportTask {
    private static final Logger LOGGER = LoggerFactory.getLogger(StockIndexImportTask.class);

    @Autowired
    private StockIndexCollector collector;
    @Autowired
    private StockIndexRepository repository;
    @Autowired
    private ApplicationProperties properties;

    @Scheduled(initialDelay = 3000, fixedRate = 300000)
    public void importStockIndices() {
        LOGGER.info("Starting import task");
        List<StockIndex> setIndices = collector.retrieveStockIndices();

        repository.save(setIndices.stream()
                .filter(index -> properties.getFilterStockIndices().contains(index.getName()))
                .collect(Collectors.toList()));
    }
}
