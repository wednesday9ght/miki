package com.miki.assignment.resource;

import com.miki.assignment.domain.StockIndex;
import com.miki.assignment.repository.StockIndexRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by worawat on 3/25/2017 AD.
 */
@RestController
public class StockIndicesResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(StockIndicesResource.class);

    @Autowired
    private StockIndexRepository repository;

    @RequestMapping(value = "/indices/{name}", method = RequestMethod.GET)
    public List<StockIndex> getTodayIndicesByName(@PathVariable String name) {
        LocalDateTime now = LocalDateTime.now();
        return repository.findAllByNameAndRetrievedDateBetween(name, now.toLocalDate().atStartOfDay(), now);
    }

    @RequestMapping(value = "/indices", method = RequestMethod.PUT)
    public StockIndex updateLast(@RequestBody StockIndex stockIndex) {
        StockIndex entity = repository.findByNameAndRetrievedDate(stockIndex.getName(), stockIndex.getRetrievedDate());
        if (entity == null) {
            throw new EmptyResultDataAccessException("Resource not found", 1);
        }
        entity.setLast(stockIndex.getLast());
        return repository.save(entity);
    }
}
