package com.miki.assignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableJpaAuditing
@EnableScheduling
@EnableTransactionManagement
@EntityScan(basePackageClasses = { StockIndicesApplication.class, Jsr310JpaConverters.class})
public class StockIndicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(StockIndicesApplication.class, args);
	}
}
