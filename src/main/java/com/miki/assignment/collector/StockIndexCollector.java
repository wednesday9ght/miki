package com.miki.assignment.collector;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTableRow;
import com.miki.assignment.config.ApplicationProperties;
import com.miki.assignment.domain.StockIndex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by worawat on 3/25/2017 AD.
 */
@Component
public class StockIndexCollector {
    private static final Logger LOGGER = LoggerFactory.getLogger(StockIndexCollector.class);

    private ApplicationProperties properties;

    @Autowired
    public StockIndexCollector(ApplicationProperties properties) {
        this.properties = properties;
    }

    public List<StockIndex> retrieveStockIndices() {
        WebClient client = new WebClient();
        client.getOptions().setCssEnabled(false);
        client.getOptions().setJavaScriptEnabled(false);
        LocalDateTime now = LocalDateTime.now();
        List<StockIndex> setIndices = new ArrayList<>();
        try {
            String url = properties.getStockIndicesUrl();
            HtmlPage page = client.getPage(url);
            String selectIndexRowsXPath = "//div[@class='table-responsive']/table[@class='table-info']/tbody/tr";
            List<HtmlTableRow> elements = (List<HtmlTableRow>) page.getByXPath(selectIndexRowsXPath);
            LOGGER.debug("Found elements: {}", elements);
            if (!elements.isEmpty()) {
                for (HtmlTableRow row : elements) {
                    String index = row.getCell(0).getTextContent().trim();
                    String lastValue = row.getCell(1).getTextContent().trim();
                    LOGGER.debug("Index: {}, last: {}", index, lastValue);
                    setIndices.add(buildStockIndex(index, lastValue, now));
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error extracting set indicies", e);
        }
        return setIndices;
    }

    private StockIndex buildStockIndex(String index, String lastValue, LocalDateTime retrievedDate) {
        Double last = null;
        try {
            Number number = NumberFormat.getInstance(Locale.ENGLISH).parse(lastValue);
            last = number.doubleValue();
        } catch (Exception e) {
            LOGGER.warn("Last value is not valid number");
        }
        return new StockIndex(index, last, retrievedDate);
    }
}
