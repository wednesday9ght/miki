package com.miki.assignment.repository;

import com.miki.assignment.domain.StockIndex;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by worawat on 3/25/2017 AD.
 */
public interface StockIndexRepository extends JpaRepository<StockIndex, Long> {

    List<StockIndex> findAllByNameAndRetrievedDateBetween(String name, LocalDateTime fromDate, LocalDateTime toDate);

    StockIndex findByNameAndRetrievedDate(String name, LocalDateTime date);
}
