package com.miki.assignment.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by worawat on 3/25/2017 AD.
 */
@Component
@ConfigurationProperties("application")
public class ApplicationProperties {
    private String stockIndicesUrl;
    private List<String> filterStockIndices;

    public String getStockIndicesUrl() {
        return stockIndicesUrl;
    }

    public void setStockIndicesUrl(String stockIndicesUrl) {
        this.stockIndicesUrl = stockIndicesUrl;
    }

    public List<String> getFilterStockIndices() {
        return filterStockIndices;
    }

    public void setFilterStockIndices(List<String> filterStockIndices) {
        this.filterStockIndices = filterStockIndices;
    }
}
