# README #

### What is this repository for? ###

Miki Assignment TEST-801

### How to setup? ###

Build project using gradle
```
#!
gradlew assemble
```

To start the server (port 8080)

```
#!
java -jar -Dspring.profiles.active=development ./build/libs/stock-indices-0.0.1-SNAPSHOT.jar
```

The value of spring.profiles.active can be development or production.


### How to test webservice? ###

The api require basic authentication. Please use the username miki and password secret123

** 1. Listing stock indices:**

*/indices/{index}*

Where index is the index name including SET, SET50, and SET100.

Example:
```
#!
curl -X GET -H "Authorization: Basic bWlraTpzZWNyZXQxMjM=" -H "Cache-Control: no-cache" -H "Postman-Token: c463ad1d-89f7-e3ae-7c33-d1874d73cb88" "http://localhost:8080/indices/SET100"
```

** 2. Update stock index last value:**

*/indices*

Example:

```
#!
curl -X PUT -H "Authorization: Basic bWlraTpzZWNyZXQxMjM=" -H "Content-Type: application/json" -H "Cache-Control: no-cache" -H "Postman-Token: c60f50a8-d68d-fe22-7ffa-977cc647e8b8" -d '{
	"name": "SET100",
	"last": 1090.91,
	"retrievedDate": "2017-03-25T22:33:18"
}' "http://localhost:8080/indices"
```

The required parameters are:
* name: stock index name
* last: last value to update
* retrievedDate: retrieved date time